future==0.18.2
requests==2.28.1
stevedore==4.0.2
git+https://gitlab.com/xivo.solutions/xivo-lib-rest-client.git
