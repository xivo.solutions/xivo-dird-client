#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo_dird_client',
    version='0.1',

    description='a simple client library for the xivo-dird HTTP interface',

    author='Avencall',
    author_email='dev@avencall.com',

    url='https://github.com/xivo-pbx/xivo-dird-client',

    packages=find_packages(),

    entry_points={
        'dird_client.commands': [
            'directories = xivo_dird_client.commands.directories:DirectoriesCommand',
            'personal = xivo_dird_client.commands.personal:PersonalCommand',
        ],
    }
)
